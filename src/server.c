#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "winutils.h"

int
main()
{
	const char *path = "\\\\.\\pipe\\testpipe";
	puts(path);

	HANDLE pip;
	pip = CreateNamedPipe(
		path, 
		PIPE_ACCESS_DUPLEX,
		PIPE_TYPE_MESSAGE,
		PIPE_UNLIMITED_INSTANCES,
		0,
		0,
		0, 
		NULL
	);
	if (pip == INVALID_HANDLE_VALUE) {
		doerror("Error creating named pipe");
	}

	int good;
	good = ConnectNamedPipe(pip, NULL);
	if (!good) doerror("Error connecting client to named pipe");

	DWORD br = 0;
	char ibuf[200] = { 0 }, obuf[200] = { 0 };

	const char *strarr[] = {
		"Hello, client! How are you?",
		"Just as well.",
		"...",
		NULL,
	}, **str = strarr;

	while (*str) {
		good = ReadFile(pip, ibuf, sizeof(ibuf), &br, NULL);
		if (!good) {
			doerror("Error reading pipe");
			break;
		}
		printf("Client: %s\n", ibuf);
		sprintf(obuf, "%s", *str);
		printf("Server: %s\n", obuf);
		good = WriteFile(pip, obuf, sizeof(obuf), &br, NULL);
		if (!good) {
			doerror("Error writing pipe");
			break;
		}
		++str;
	}

	CloseHandle(pip);
	return 0;
}
