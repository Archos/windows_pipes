#include <stdio.h>
#include <stdlib.h>
#include <utils.h>
#include <windows.h>
#include "winutils.h"

int
main()
{
	const char *path = "\\\\.\\pipe\\testpipe";
	HANDLE pipe;
	int canplumb = WaitNamedPipe(path, NMPWAIT_WAIT_FOREVER);
	if (canplumb) {
		DWORD pipemode = PIPE_READMODE_MESSAGE;
		printf("Connecting to %s...\n", path);
		pipe = CreateFile(path, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		SetNamedPipeHandleState(pipe, &pipemode, NULL, NULL);
	} else {
		fprintf(stderr, "Error: No plumbing to be done today.\n");
		return 1;
	}

	DWORD br = 0;
	char ibuf[200] = { 0 }, obuf[200] = { 0 };

	const char *strarr[] = {
		"Hello, server!",
		"I'm good. And you?",
		"Excellent.",
		NULL,
	}, **str = strarr;

	int good;
	while (*str) {
		sprintf(obuf, "%s", *str);
		printf("Client: %s\n", obuf);
		good = TransactNamedPipe(
			pipe,
			obuf,
			sizeof(obuf),
			ibuf,
			sizeof(ibuf),
			&br,
			NULL
		);
		if (!good) {
			doerror("Error calling named pipe");
			break;
		}
		printf("Server: %s\n", ibuf);
		++str;
	}

	return 0;
};
