build: obj/server.o obj/client.o obj/winutils.o
	cc -o server.exe obj/server.o obj/winutils.o
	cc -o client.exe obj/client.o obj/winutils.o

obj/server.o: src/server.c obj/
	cc -c -o $@ $<

obj/client.o: src/client.c obj/
	cc -c -o $@ $<

obj/winutils.o: src/winutils.c obj/
	cc -c -o $@ $<

obj/:
	mkdir obj

clean:
	rm -rf obj *.exe
